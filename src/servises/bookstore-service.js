export default class BookstoreService {

  data = [
    {
      id: 1,
      title: 'Чистый код. Создание, анализ и рефакторинг',
      author: 'Мартин Роберт К.',
      price: 484,
      coverImage: 'https://cdn1.ozone.ru/multimedia/c360/1001563239.jpg',
    },
    {
      id: 2,
      title: 'Грокаем алгоритмы. Иллюстрированное пособие для программистов и любопытствующих',
      author: 'Бхаргава Адитья',
      price: 799,
      coverImage: 'https://cdn1.ozone.ru/multimedia/1037901676.jpg',
    },
  ];

  getBooks() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (Math.random() > 0.75) {
          reject(new Error('Something bad happened'));
        } else {
          resolve(this.data);
        }
      }, 700);
    });
  }

}