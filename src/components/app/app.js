import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { CartPage, HomePage } from '../pages';

import './app.css';
import ShopHeader from '../shop-header';

const App = (bookstoreService) => {
  // const { cartItems, orderTotal } = this.props;
  return (
    <main role="main" className="container">
      <ShopHeader />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/cart" element={<CartPage />} />
      </Routes>
    </main>
  );
};

export default App;