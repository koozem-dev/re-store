import React from 'react';

const ErrorIndicator = () => {
  return (
    <span>Oops, it's some error! Try again later.</span>
  );
}

export default ErrorIndicator;